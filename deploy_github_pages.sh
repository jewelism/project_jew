#!/usr/bin/env sh

npm run build

cd www

git config user.name "jewelism"
git config user.email "boseokjung@gmail.com"

touch .nojekyll

git init
git add -A
git commit -m 'deploy'

git push -f git@github.com:jewelism/project_jew.git master

cd -