import { debounce } from "~/utils";

export const createPlayerMovement = (scene, key) => {
  // const { clientWidth, clientHeight } = getMainCameraSize();
  scene.anims.create({
    key: 'horizontal',
    frames: scene.anims.generateFrameNumbers(key, { start: 0, end: 2 }),
    frameRate: 5,
    repeat: -1,
  });
  scene.anims.create({
    key: 'vertical',
    frames: [{ key: key, frame: 3 }, { key: key, frame: 4 }, { key: key, frame: 5 }],
    frameRate: 5,
    repeat: -1,
  });

  scene.anims.create({
    key: 'idle',
    frames: [{ key: key, frame: 1 }],
    frameRate: 5,
  });

  const player = scene.physics.add.sprite(40, 80, key);
  player.setScale(2.0);
  player.setCollideWorldBounds(true);
  scene.cameras.main.followOffset.set(-300, 0);
  // const player = scene.add.sprite(clientWidth / 2, clientHeight / 2, key);
  const cursors = scene.input.keyboard.createCursorKeys();
  return { player, cursors };
};

export const playerMovement = ({ scene, speed = 1 }) => {
  // following camera
  // const {halfWidth, halfHeight} = getGameSize();
  // scene.cameras.main.scrollX = scene.player.x - halfWidth;
  // scene.cameras.main.scrollY = scene.player.y - halfHeight;
  scene.player.setVelocity(0);
  scene.player.anims.play('idle');
  if (scene.cursors.left.isDown) {
    scene.player.anims.play('horizontal', true);
    scene.player.setVelocityX(-speed);
  } else if (scene.cursors.right.isDown) {
    scene.player.anims.play('horizontal', true);
    scene.player.setVelocityX(speed);
  }
  if (scene.cursors.up.isDown) {
    scene.player.anims.play('vertical', true);
    scene.player.setVelocityY(-speed);
  } else if (scene.cursors.down.isDown) {
    scene.player.anims.play('vertical', true);
    scene.player.setVelocityY(speed);
  }
};

export const playerKeyDown = ({ scene, speed }) => {
  const keyA = scene.input.keyboard.addKey('A');
  if (scene.input.keyboard.checkDown(keyA, speed)) {
    console.log('key a');
  }
}