export const getGameSize = () => {
  const { width, height } = window.game.config;
  return {
    width: Number(width),
    height: Number(height),
    halfWidth: Number(width) / 2,
    halfHeight: Number(height) / 2
  };
}

export const getMainCameraSize = () => {
  const { clientWidth, clientHeight } = document.documentElement;
  return { clientWidth, clientHeight };
}