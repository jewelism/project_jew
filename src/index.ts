import MainScene from '~/scenes/MainScene';

const config = {
  type: Phaser.WEBGL,
  // width: document.documentElement.clientWidth,
  // height: document.documentElement.clientHeight,
  width: 2000,
  height: 1000,
  backgroundColor: '#074632',
  physics: {
    default: 'arcade',
    // default: 'matter',
    // matter: {
    //   enabled: true,
    // },
  },
  scene: [MainScene],
};

window.game = new Phaser.Game(config);

