// import SkyImage from '~/assets/sky.png';
import ManImage from '~/assets/man.png';
import TreeImage from '~/assets/tree-icon.svg';
import { createPlayerMovement, playerKeyDown, playerMovement } from '~/objects/player';
import { getGameSize, getMainCameraSize } from '~/utils/helper';
import { getRandomArbitrary } from '~/utils';

export default class MainScene extends Phaser.Scene {
  player: any;
  cursors: any;
  minimap: Phaser.Cameras.Scene2D.Camera;

  constructor(scene) {
    super(scene);
  }

  preload() {
    // this.load.image('bg', SkyImage);
    this.load.spritesheet('man', ManImage,{
      frameWidth: 24, frameHeight: 32, 
    });
    this.load.svg('tree', TreeImage, { scale: 3 });
  }

  create() {
    // this.add.image(0, 0, 'bg').setOrigin(0);
    const { width, height } = getGameSize();
    this.cameras.main.setBounds(0, 0, width, height);
    this.physics.world.setBounds(0, 0, width, height);

    const { clientWidth, clientHeight } = getMainCameraSize();
    this.cameras.main.setSize(clientWidth, clientHeight);

    const { player, cursors } = createPlayerMovement(this, 'man');
    this.player = player;
    this.cursors = cursors;
    // console.log('this.cameras.main', this.cameras.main, this.cameras.main.setSize, Phaser.GameObjects);
    const trees = Array.from({ length: 100 }).map(() => {
      const treeX = getRandomArbitrary(100, width);
      const treeY = getRandomArbitrary(100, height);
      const tree = this.physics.add.staticImage(treeX, treeY, 'tree');
      tree.refreshBody();
      return tree;
    });
    this.physics.add.collider(player, trees);

    this.cameras.main.startFollow(player);
  }

  update() {
    playerMovement({ scene: this, speed: 300 });
    playerKeyDown({ scene: this, speed: 1000 });
  }
}
