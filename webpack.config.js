const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const TerserPlugin = require('terser-webpack-plugin');

const resolve = (dir) => path.resolve(__dirname, '../', dir);

module.exports = {
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, 'www'),
    filename: '[name].bundle.[hash].js',
  },
  resolve: {
    extensions: ['.ts', '.js', '.png'],
    alias: {
      '~': resolve('/src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|svg|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'assets/[name].[ext]',
        },
      },
    ],
  },
  externals: {
    Phaser: 'phaser',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, './src/index.html'),
      inject: true,
      filename: path.join(__dirname, './www/index.html'),
    }),
    new webpack.ProvidePlugin({
      Phaser: 'phaser',
    }),
  ],
  // optimization: {
  //   minimize: true,
  //   minimizer: [new TerserPlugin()],
  // },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    inline: true,
    hot: true,
    host: 'localhost',
    port: 9000,
  },
  devtool: 'source-map',
  mode: 'development',
  // mode: 'production'
};
