import * as _Phaser from 'phaser';

declare global {
  interface Window {
    Phaser: typeof _Phaser;
    game: _Phaser.Game;
  }
}
